defmodule NasaTest do
  use ExUnit.Case
  doctest Nasa

  test "calculate fuel to land on earth with equipment weight 28801" do
    assert Nasa.calculate_fuel(28801, [{:land, 9.807}]) == 13447
  end

  test "0 fuel for 0 mass" do
    assert Nasa.calculate_fuel(0, [{:land, 9.807}]) == 0
  end

  test "[] returns 0" do
    assert Nasa.calculate_fuel(12345, []) == 0
  end

  test "Apolo 11" do
    assert Nasa.calculate_fuel(28801, [
             {:launch, 9.807},
             {:land, 1.62},
             {:launch, 1.62},
             {:land, 9.807}
           ]) == 51898
  end

  test "Mission on Mars" do
    assert Nasa.calculate_fuel(14606, [
             {:launch, 9.807},
             {:land, 3.711},
             {:launch, 3.711},
             {:land, 9.807}
           ]) == 33388
  end

  test "Passenger ship" do
    assert Nasa.calculate_fuel(75432, [
             {:launch, 9.807},
             {:land, 1.62},
             {:launch, 1.62},
             {:land, 3.711},
             {:launch, 3.711},
             {:land, 9.807}
           ]) == 212_161
  end
end
