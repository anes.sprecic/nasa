defmodule Nasa do
  def calculate_fuel(equipment_weight, list) do
    List.foldl(Enum.reverse(list), 0, fn manouver, acc ->
      acc + calculate_fuel_for_mass(equipment_weight + acc, manouver)
    end)

    # Recursive solution
    # calculate_fuel_recursive(equipment_weight, Enum.reverse(list))
  end

  defp calculate_fuel_for_mass(mass, _tuple) when mass <= 0 do
    0
  end

  defp calculate_fuel_for_mass(mass, {:launch, gravity} = tuple) do
    fuel = (mass * gravity * 0.042 - 33) |> Float.floor() |> clamp_to_zero
    fuel + calculate_fuel_for_mass(fuel, tuple)
  end

  defp calculate_fuel_for_mass(mass, {:land, gravity} = tuple) do
    fuel = (mass * gravity * 0.033 - 42) |> Float.floor() |> clamp_to_zero
    fuel + calculate_fuel_for_mass(fuel, tuple)
  end

  defp clamp_to_zero(num) when num <= 0 do
    0
  end

  defp clamp_to_zero(num) do
    num
  end

  ## Recursive solution
  defp calculate_fuel_recursive(equipment_weight, _list) when equipment_weight <= 0 do
    0
  end

  defp calculate_fuel_recursive(_equipment_weight, []) do
    0
  end

  defp calculate_fuel_recursive(equipment_weight, [manouver | rest]) do
    fuel = calculate_fuel_for_mass(equipment_weight, manouver)
    fuel + calculate_fuel_recursive(equipment_weight + fuel, rest)
  end
end
