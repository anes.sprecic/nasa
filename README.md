# Nasa

Run `mix test` to verify module is working correctly.

Interface of module exports only one function: `Nasa.calculate_fuel(weight, [manouvers]`

Example usage:
```
iex(1)> Nasa.calculate_fuel(28801, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}])
51898.0
```

Inside lib/nasa.ex you can find an additional recursive solution, which you can use if you want to.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `nasa_fuel` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:nasa_fuel, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/nasa_fuel>.

